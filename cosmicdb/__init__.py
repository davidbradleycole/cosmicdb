VERSION = (0, 0, 33)


def get_version():
    """Return the version as a string."""
    return '.'.join(map(str, VERSION))
